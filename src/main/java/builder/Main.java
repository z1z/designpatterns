package builder;

public class Main {

    public static void main(String[] args) {
	    Room room1 = new Room.RoomBuilder("Zal").
                withSofa(new Sofa("Germany")).
                withTv(new Tv("Samsung")).
                build();
        System.out.println(room1);

        Room room2 = new Room.RoomBuilder("Kitchen").
                withChair(new Chair("Wooden")).
                build();
        System.out.println(room2);

        Room room3 = new Room.RoomBuilder("Detskaya").
                withSofa(new Sofa("Germany")).
                withTv(new Tv("Samsung")).
                withChair(new Chair("Wooden")).
                build();
        System.out.println(room3);
    }
}
