package builder;

public class Sofa {
    private String madeIn;

    public Sofa(String madeIn) {
        setMadeIn(madeIn);
    }

    public String getMadeIn() {
        return madeIn;
    }

    public void setMadeIn(String madeIn) {
        this.madeIn = madeIn;
    }

    @Override
    public String toString() {
        return madeIn;
    }
}
