package builder;

public class Tv {
    private String company;

    public Tv(String company) {
        setCompany(company);
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    @Override
    public String toString() {
        return company;
    }
}
