package builder;

public class Chair {
    private String material;

    public Chair(String material) {
        setMaterial(material);
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    @Override
    public String toString() {
        return material;
    }
}
