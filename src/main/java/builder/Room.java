package builder;

public class Room {
    private String name;
    private Sofa sofa;
    private Tv tv;
    private Chair chair;

    private Room(RoomBuilder builder) {
        setName(builder.name);
        setSofa(builder.sofa);
        setTv(builder.tv);
        setChair(builder.chair);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Sofa getSofa() {
        return sofa;
    }

    public void setSofa(Sofa sofa) {
        this.sofa = sofa;
    }

    public Tv getTv() {
        return tv;
    }

    public void setTv(Tv tv) {
        this.tv = tv;
    }

    public Chair getChair() {
        return chair;
    }

    public void setChair(Chair chair) {
        this.chair = chair;
    }

    public static class RoomBuilder {
        private String name;
        private Sofa sofa;
        private Tv tv;
        private Chair chair;

        public RoomBuilder(String name) {
            this.name = name;
        }

        public RoomBuilder withSofa(Sofa sofa) {
            this.sofa = sofa;
            return this;
        }

        public RoomBuilder withTv(Tv tv) {
            this.tv = tv;
            return this;
        }

        public RoomBuilder withChair(Chair chair) {
            this.chair = chair;
            return this;
        }

        public Room build(){
            return new Room(this);
        }
    }
    @Override
    public String toString() {
        return name+' '+sofa+' '+tv+' '+chair;
    }
}
