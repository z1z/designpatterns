package adapter;

public class Main {
    public static void main(String[] args) {
        Disk disk = new Disk("Some data is here!");
        DiskDriver diskDriver = new Computer(disk);
        diskDriver.insertDisk();
    }
}
