package adapter;

public class Computer implements DiskDriver {

    private Disk disk;

    public Computer (Disk disk){
        this.disk = disk;
    }

    public void insertDisk() {
        System.out.println("Disk successfully inserted!"+disk.getData());
    }
}
