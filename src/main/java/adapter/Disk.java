package adapter;

public class Disk {
    private String data;

    public Disk() {
    }

    public Disk(String data) {
        setData(data);
    }

    public String getData() {
        return "Data in the disk: "+data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return data;
    }
}
